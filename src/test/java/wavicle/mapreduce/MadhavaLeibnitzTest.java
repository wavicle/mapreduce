package wavicle.mapreduce;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import wavicle.mapreduce.MapReduceJob.KeyAndValue;

public class MadhavaLeibnitzTest {

	@Test
	public void test() {
		List<Double> inputs = new ArrayList<>();
		for (double i = 0; i <= 1000; i++) {
			inputs.add(i);
		}

		MapReduceJob job = new MapReduceJob() {
			@Override
			protected void map(Object key, Object value, Collector collector) {
				double i = (double) value;
				double denom = 2 * i + 1;
				int sign = (i % 2) == 0 ? 1 : -1;
				double termValue = sign * (4 / denom);
				collector.collect("terms", termValue);
			}

			@Override
			protected void reduce(Object key, List<? extends Object> values, Collector collector) {
				double sum = 0;
				for (Object val : values) {
					sum += (double) val;
				}
				collector.collect("pi", sum);
			}
		};

		List<KeyAndValue> outputs = job.run(inputs);

		double calculatedPi = (double) outputs.get(0).value;
		assertEquals(3.14, calculatedPi, 0.01);
	}
}
