package wavicle.mapreduce;

import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import org.junit.Test;

import wavicle.mapreduce.MapReduceJob.KeyAndValue;

public class WordCountTest {
	@Test
	public void test() {
		List<?> inputs = Arrays.asList("The capital of Japan is Tokyo", "Tokyo Drift is a great song",
				"Programming is fun even if it is tedious sometimes", "MapReduce is really cool");

		MapReduceJob job = new MapReduceJob() {
			@Override
			protected void map(Object key, Object value, Collector collector) {
				String line = (String) value;
				StringTokenizer tokenizer = new StringTokenizer(line);
				while (tokenizer.hasMoreTokens()) {
					String word = tokenizer.nextToken();
					collector.collect(word, 1);
				}
			}

			@Override
			protected void reduce(Object key, List<? extends Object> values, Collector collector) {
				collector.collect(key, values.size());
			}
		};

		List<KeyAndValue> outputs = job.run(inputs);
		System.out.println("Outputs: " + outputs);
	}
}
