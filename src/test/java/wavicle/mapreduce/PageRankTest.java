package wavicle.mapreduce;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import wavicle.mapreduce.MapReduceJob.KeyAndValue;

/**
 * A simple example to compute page-ranks using map/reduce operations. This test
 * uses the example provided here:
 * 
 * http://pr.efactory.de/e-pagerank-algorithm.shtml
 * 
 * @author wavicle
 *
 */
public class PageRankTest {

	private static final double DAMPING_FACTOR = 0.5;

	@SuppressWarnings("unchecked")
	@Test
	public void test() {
		List<KeyAndValue> inputs = new ArrayList<>();
		inputs.add(new KeyAndValue(Arrays.asList("a", 0.1), Arrays.asList("b", "c")));
		inputs.add(new KeyAndValue(Arrays.asList("b", 0.1), Arrays.asList("c")));
		inputs.add(new KeyAndValue(Arrays.asList("c", 0.1), Arrays.asList("a")));

		MapReduceJob job = new MapReduceJob() {
			@Override
			protected void map(Object key, Object value, Collector collector) {
				List<Object> keyObjects = (List<Object>) key;
				String url = (String) keyObjects.get(0);
				double pageRank = (double) keyObjects.get(1);
				List<String> outlinks = (List<String>) value;
				for (String outlink : outlinks) {
					collector.collect(outlink, pageRank / outlinks.size());
				}
				collector.collect(url, outlinks);
			}

			@Override
			protected void reduce(Object key, List<? extends Object> values, Collector collector) {
				String url = (String) key;
				double pageRank = 0;
				List<String> outlinks = new ArrayList<>();
				for (Object prOrUrls : values) {
					if (prOrUrls instanceof Double) {
						pageRank += (double) prOrUrls;
					} else {
						outlinks = (List<String>) prOrUrls;
					}
				}
				pageRank = 1 - DAMPING_FACTOR + (DAMPING_FACTOR * pageRank);
				collector.collect(Arrays.asList(url, pageRank), outlinks);
			}
		};

		for (int i = 0; i < 10000; i++) {
			inputs = job.run(inputs);
		}

		double totalPageRank = 0;
		for (KeyAndValue kv : inputs) {
			List<Object> keyObjects = (List<Object>) kv.key;
			totalPageRank += (double) keyObjects.get(1);
		}

		assertEquals(inputs.size(), totalPageRank, 0.01);
	}
}
