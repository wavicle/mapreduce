package wavicle.mapreduce;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;

public abstract class MapReduceJob {
	public static class KeyAndValue {
		final Object key, value;

		public KeyAndValue(Object key, Object value) {
			super();
			this.key = key;
			this.value = value;
		}

		@Override
		public String toString() {
			return ToStringBuilder.reflectionToString(this);
		}
	}

	protected abstract void map(Object key, Object value, Collector collector);

	protected abstract void reduce(Object key, List<? extends Object> values, Collector collector);

	public List<KeyAndValue> run(List<? extends Object> inputs) {
		int i = 0;
		Collector mapperCollector = new Collector();
		for (Object input : inputs) {
			Object key, value;
			if (input instanceof KeyAndValue) {
				KeyAndValue kv = (KeyAndValue) input;
				key = kv.key;
				value = kv.value;
			} else {
				key = i;
				value = input;
			}
			map(key, value, mapperCollector);
		}

		Map<Object, List<Object>> groupedMapperOutputs = new HashMap<>();
		for (KeyAndValue kv : mapperCollector.getList()) {
			Object key = kv.key;
			Object value = kv.value;
			if (!groupedMapperOutputs.containsKey(key)) {
				groupedMapperOutputs.put(key, new ArrayList<>());
			}
			groupedMapperOutputs.get(key).add(value);
		}

		Collector reducerCollector = new Collector();
		for (Object key : groupedMapperOutputs.keySet()) {
			List<Object> values = groupedMapperOutputs.get(key);
			reduce(key, values, reducerCollector);
		}
		return reducerCollector.getList();
	}
}
