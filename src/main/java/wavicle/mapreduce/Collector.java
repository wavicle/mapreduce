package wavicle.mapreduce;

import java.util.ArrayList;
import java.util.List;

import wavicle.mapreduce.MapReduceJob.KeyAndValue;

public class Collector {
	private List<KeyAndValue> list = new ArrayList<>();

	public void collect(Object key, Object value) {
		list.add(new KeyAndValue(key, value));
	}

	public List<KeyAndValue> getList() {
		return list;
	}
}
